using System.IO;
using System.Threading.Tasks;
using SharedArea;
using SharedArea.Answers;
using SharedArea.Questions;

namespace DataKeeperPeer
{
    public class Consumer : IResponder<AskTest, AnswerTest>, IResponder<AskRecogObjects, AnswerRecogObjects>
    {
        private static readonly string PhotosDir = "DeepVision/Storage";
        
        public AnswerTest AnswerQuestion(AnswerContext<AskTest> question)
        {
            return new AnswerTest() {MsgText = $"Hello {question.Question.Name} !"};
        }

        public AnswerRecogObjects AnswerQuestion(AnswerContext<AskRecogObjects> question)
        {
            return new AnswerRecogObjects()
                {Boxes = EyeLib.Program.ScanImage(Path.Combine(PhotosDir, question.Question.PhotoId + ".jpg"))};
        }
    }
}
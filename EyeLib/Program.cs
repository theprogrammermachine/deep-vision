﻿using System;
using System.Collections.Generic;
using SharedArea.Models;

namespace EyeLib
{
    public class Program
    {
        private const string ModelFilePath = "/home/keyhan/projects/dotnet/ObjectRecognitionCloud/EyeLib/TinyYoloV2.onnx";
        
        public static List<YoloBoundingBox> ScanImage(string path)
        {
            try
            {
                return new Yolo(ModelFilePath).Score(path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            return new List<YoloBoundingBox>();
        }
    }
}
﻿using System.Drawing;
using Microsoft.ML.Data;

namespace EyeLib
{
    public class ImageNetData
    {
        [LoadColumn(0)]
        public string ImagePath;

        [LoadColumn(1)]
        public string Label;
    }

    public class ImageNetDataProbability : ImageNetData
    {
        public string PredictedLabel;
        public float Probability { get; set; }
    }
    
    public class ImageNetPrediction
    {
        [ColumnName(Yolo.TinyYoloModelSettings.ModelOutput)]
        public float[] PredictedLabels;
    }
}
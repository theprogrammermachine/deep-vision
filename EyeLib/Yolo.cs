﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ML;
using SharedArea.Models;

namespace EyeLib
{
    class Yolo
    {
        private readonly string modelLocation;
        private readonly MLContext mlContext;
        private PredictionEngine<ImageNetData, ImageNetPrediction> engine;
        private IList<YoloBoundingBox> _boundingBoxes = new List<YoloBoundingBox>();
        private readonly YoloDecoder _parser = new YoloDecoder();

        public Yolo(string modelLocation)
        {
            this.modelLocation = modelLocation;
            mlContext = new MLContext();
            this.engine = LoadModel(modelLocation);
        }

        public struct ImageNetSettings
        {
            public const int imageHeight = 416;
            public const int imageWidth = 416;
        }

        public struct TinyYoloModelSettings
        {
            public const string ModelInput = "image";
            public const string ModelOutput = "grid";
        }

        public List<YoloBoundingBox> Score(string imagePath)
        {
            Console.WriteLine("Hello ..................................................................................");
            return PredictDataUsingModel(imagePath);
        }

        private PredictionEngine<ImageNetData, ImageNetPrediction> LoadModel(string modelLocation)
        {
            var data = CreateEmptyDataView();

            var pipeline = mlContext.Transforms.LoadImages(outputColumnName: "image", imageFolder: "", 
                    inputColumnName: nameof(ImageNetData.ImagePath))
                            .Append(mlContext.Transforms.ResizeImages(outputColumnName: "image", imageWidth: 
                    ImageNetSettings.imageWidth, imageHeight: ImageNetSettings.imageHeight, inputColumnName: "image"))
                            .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "image"))
                            .Append(mlContext.Transforms.ApplyOnnxModel(modelFile: modelLocation, outputColumnNames: 
                    new[] { TinyYoloModelSettings.ModelOutput }, inputColumnNames: new[] { TinyYoloModelSettings.ModelInput }));
            
            var model = pipeline.Fit(data);

            var predictionEngine = mlContext.Model.CreatePredictionEngine<ImageNetData, ImageNetPrediction>(model);

            return predictionEngine;
        }

        protected List<YoloBoundingBox> PredictDataUsingModel(string imagePath)
        {
            var sample = new ImageNetData() {ImagePath = imagePath, Label = "1"};            
            var probs = this.engine.Predict(sample).PredictedLabels;
            _boundingBoxes = _parser.Decode(probs);
            var filteredBoxes = _parser.NonMaxSuppress(_boundingBoxes, 5, .5F);
            Console.WriteLine(".....The objects in the image {0} are detected as below....", sample.Label);
            foreach (var box in filteredBoxes)
            {
                Console.WriteLine(box.Label + " and its Confidence score: " + box.Confidence + 
                                  ", box props : [ x : " + box.X + 
                                  ", y : " + box.Y + 
                                  ", width : " + box.Width +
                                  ", height : " + box.Height + 
                                  " ] on 416 * 416 image.");
            }
            return filteredBoxes.ToList();
        }

        private IDataView CreateEmptyDataView()
        {
            List<ImageNetData> list = new List<ImageNetData>();
            IEnumerable<ImageNetData> enumerableData = list;
            var dv = mlContext.Data.LoadFromEnumerable(enumerableData);
            return dv;
        }
    }
}


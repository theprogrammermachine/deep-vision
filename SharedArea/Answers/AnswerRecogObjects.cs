using System.Collections.Generic;
using SharedArea.Models;

namespace SharedArea.Answers
{
    public class AnswerRecogObjects
    {
        public List<YoloBoundingBox> Boxes { get; set; }
    }
}
using SharedArea.Models;

namespace SharedArea.Questions
{
    public class AskDownloadFile
    {
        public string StreamCode { get; set; }
        public long Offset { get; set; }
        public Photo Photo { get; set; }
    }
}
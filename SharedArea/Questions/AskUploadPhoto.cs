using System.Collections.Generic;
using SharedArea.Middles;

namespace SharedArea.Questions
{
    public class AskUploadPhoto
    {
        public Dictionary<string, string> Headers { get; set; }
        public PhotoUF Puf { get; set; }
    }
}
using System.Collections.Generic;

namespace SharedArea.Questions
{
    public class AskFileSize
    {
        public Dictionary<string, string> Headers { get; set; }
        public long FileId { get; set; }
    }
}
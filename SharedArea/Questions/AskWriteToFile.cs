using System.Collections.Generic;
using SharedArea.Models;

namespace SharedArea.Questions
{
    public class AskWriteToFile
    {
        public Dictionary<string, string> Headers { get; set; }
        public Photo Photo { get; set; }
        public string StreamCode { get; set; }
    }
}
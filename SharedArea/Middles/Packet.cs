﻿
using System.Collections.Generic;
using SharedArea.Models;

namespace SharedArea.Middles
{
    public class Packet
    {
        public string Status { get; set; }
        public Photo Photo { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string StreamCode { get; set; }
        public bool? FinishFileTransfer { get; set; }
        public long? Offset { get; set; }
        public List<YoloBoundingBox> Boxes { get; set; }
    }
}
﻿
namespace SharedArea.Middles
{
    public class PhotoUF
    {
        public long ComplexId { get; set; }
        public long RoomId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool IsAvatar { get; set; }
        public bool AsRawFile { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace SharedArea.Middles
{
    public class ModuleResponse
    {
        public Dictionary<string, string> Parameters { get; set; }
    }
}
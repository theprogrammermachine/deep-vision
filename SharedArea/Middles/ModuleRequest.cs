﻿using System.Collections.Generic;

namespace SharedArea.Middles
{
    public class ModuleRequest
    {
        public string ActionName { get; set; }
        public Dictionary<string, string> Parameters { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace SharedArea
{
    public class GlobalVariables
    {
        public static readonly Dictionary<string, string> SuperPeerAddresses = new Dictionary<string, string>()
        {
            { "#guilan", "localhost:9093" }
        };

        public static string KafkaUsername { get; } = "admin";
        public static string KafkaPassword { get; } = "admin-secret";
        public static string FileTransferUsername { get; } = "asdmin";
        public static string FileTransferPassword { get; } = "admin-secret";
    }
}
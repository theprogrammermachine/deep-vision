﻿using System.Drawing;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SharedArea.Models
{
    public class YoloBoundingBox
    {
        [JsonProperty("label")]
        public string Label { get; set; }
        [JsonProperty("confidence")]
        public float Confidence { get; set; }
        [JsonProperty("x")]
        public float X { get; set; }
        [JsonProperty("y")]
        public float Y { get; set; }
        [JsonProperty("width")]
        public float Width { get; set; }
        [JsonProperty("height")]
        public float Height { get; set; }

        public RectangleF Rect => new RectangleF(X, Y, Width, Height);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ApiGateway.Utils;
using Microsoft.AspNetCore.Http.Features;
using SharedArea.Middles;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;
using SharedArea.Answers;
using SharedArea.Models;
using SharedArea.Questions;

namespace ApiGateway.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : Controller
    {
        private static readonly FormOptions DefaultFormOptions = new FormOptions();

        [Route("~/api/file/write_to_file")]
        [RequestSizeLimit(bytes: 4294967296)]
        [HttpPost]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<Packet>> WriteToFile()
        {
            if (!MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
            {
                return new Packet {Status = "error_0"};
            }

            var formParts = new Dictionary<string, string>();

            var boundary = MultipartRequestHelper.GetBoundary(
                MediaTypeHeaderValue.Parse(Request.ContentType),
                DefaultFormOptions.MultipartBoundaryLengthLimit);
            var reader = new MultipartReader(boundary, HttpContext.Request.Body);

            var section = await reader.ReadNextSectionAsync();
            while (section != null)
            {
                var hasContentDispositionHeader =
                    ContentDispositionHeaderValue.TryParse(section.ContentDisposition,
                        out var contentDisposition);

                if (hasContentDispositionHeader)
                {
                    if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                    {
                        var guid = Guid.NewGuid().ToString();

                        lock (StreamRepo.GlobalLock)
                        {
                            StreamRepo.FileStreams.Add(guid, section.Body);
                        }
                        
                        var kt = new KafkaTransport();
                        var res = kt.AskPairedPeer<AskWriteToFile, AnswerWriteToFile>(
                            new AskWriteToFile()
                            {
                                Headers = Request.Headers.ToDictionary(a => a.Key, a => a.Value.ToString()),
                                Photo = new Photo() {PhotoId = Convert.ToInt64(formParts["FileId"])},
                                StreamCode = guid
                            });

                        lock (StreamRepo.GlobalLock)
                        {
                            StreamRepo.FileStreams.Remove(guid);
                        }

                        return res.Result.Packet;
                    }
                    else if (MultipartRequestHelper.HasFormDataContentDisposition(contentDisposition))
                    {
                        var key = HeaderUtilities.RemoveQuotes(contentDisposition.Name);
                        var encoding = GetEncoding(section);
                        using (var streamReader = new StreamReader(
                            section.Body,
                            encoding,
                            detectEncodingFromByteOrderMarks: true,
                            bufferSize: 1024,
                            leaveOpen: true))
                        {
                            var value = await streamReader.ReadToEndAsync();

                            formParts[key.ToString()] = value;

                            if (formParts.Count > DefaultFormOptions.ValueCountLimit)
                            {
                                throw new InvalidDataException(
                                    $"Form key count limit {DefaultFormOptions.ValueCountLimit} exceeded.");
                            }
                        }
                    }
                }

                section = await reader.ReadNextSectionAsync();
            }

            return new Packet() {Status = "error_2"};
        }

        private static Encoding GetEncoding(MultipartSection section)
        {
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out var mediaType);
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }

            return mediaType.Encoding;
        }

        [Route("~/api/file/get_file_size")]
        [HttpPost]
        public async Task<ActionResult<Packet>> GetFileSize([FromBody] Packet packet)
        {
            var kt = new KafkaTransport();
            var res = await kt.AskPairedPeer<AskFileSize, AnswerFileSize>(
                new AskFileSize() 
                    {
                        Headers = Request.Headers.ToDictionary(a => a.Key, a => a.Value.ToString()),
                        FileId = packet.Photo.PhotoId
                    });

            return res.Packet;
        }

        [Route("~/api/file/get_file_upload_stream")]
        [HttpPost]
        public ActionResult GetFileUploadStream([FromBody] Packet packet)
        {
            if (packet.Username == SharedArea.GlobalVariables.FileTransferUsername
                && packet.Password == SharedArea.GlobalVariables.FileTransferPassword)
            {
                return File(StreamRepo.FileStreams[packet.StreamCode], "application/octet-stream");
            }
            else
            {
                return NotFound();
            }
        }

        [Route("~/api/file/upload_photo")]
        [HttpPost]
        public async Task<ActionResult<Packet>> UploadPhoto([FromForm] PhotoUploadForm form)
        {
            var puf = new PhotoUF()
            {
                ComplexId = form.ComplexId,
                RoomId = form.RoomId,
                Width = form.Width,
                Height = form.Height,
                IsAvatar = form.IsAvatar
            };

            var kt = new KafkaTransport();
            var res = await kt.AskPairedPeer<AskUploadPhoto, AnswerUploadPhoto>(new AskUploadPhoto()
            {
                Puf = puf, 
                Headers = Request.Headers.ToDictionary(a => a.Key, a => a.Value.ToString())
            });
            
            return res.Packet;
        }

        [Route("~/api/file/take_file_download_stream")]
        [RequestSizeLimit(bytes: 4294967296)]
        [HttpPost]
        public ActionResult TakeFileDownloadStream([FromForm] TakeFileDSF form)
        {
            if (form.Username != SharedArea.GlobalVariables.FileTransferUsername ||
                form.Password != SharedArea.GlobalVariables.FileTransferPassword) return Forbid();
            
            var file = form.File;
            var streamCode = form.StreamCode;

            lock (StreamRepo.GlobalLock)
            {
                StreamRepo.FileStreams.Add(streamCode, file.OpenReadStream());
            }

            var lockObj = StreamRepo.FileStreamLocks[streamCode];

            lock (lockObj)
            {
                Monitor.Pulse(lockObj);
            }

            lock (lockObj)
            {
                Monitor.Wait(lockObj);
            }

            return Ok();
        }

        [Route("~/api/file/download_file")]
        [HttpGet]
        public ActionResult DownloadFile(long fileId, long offset)
        {
            var guid = Guid.NewGuid().ToString();

            var lockObj = new object();

            lock (StreamRepo.GlobalLock)
            {
                StreamRepo.FileStreamLocks.Add(guid, lockObj);
            }

            lock (lockObj)
            {
                var kt = new KafkaTransport();
                var res = kt.AskPairedPeer<AskDownloadFile, AnswerDownloadFile>(new AskDownloadFile()
                {
                    StreamCode = guid,
                    Offset = offset,
                    Photo = new Photo() {PhotoId = fileId}
                });
                    
                Monitor.Wait(lockObj);
            }

            var stream = StreamRepo.FileStreams[guid];

            Response.OnCompleted(() =>
            {
                lock (StreamRepo.GlobalLock)
                {
                    StreamRepo.FileStreams.Remove(guid);
                    StreamRepo.FileStreamLocks.Remove(guid);
                }

                lock (lockObj)
                {
                    Monitor.Pulse(lockObj);
                }

                return Task.CompletedTask;
            });

            Response.ContentLength = stream.Length;

            return File(stream, "application/octet-stream");
        }
    }
}